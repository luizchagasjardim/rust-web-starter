* add email to user
* add a create account page with email confirmation
* implement "forgot password" mechanism
* save login attempts table in redis
    * if a user gets the password wrong twice, he gets a warning email
    * three or more times, block that user agent for a while?
* stop using rocket_session. create own implementation
* write a million unit tests
* admin page
* never delete an user through the api, just mark as deleted