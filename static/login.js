function on_load() {
    document.getElementById("login_form").addEventListener("submit", function(event) {
        //form submit does not support json
        event.preventDefault();
        login();
    });
}

function login() {
    let username = document.getElementById("username").value;
    let password = document.getElementById("password").value;
    $.ajaxSetup({
        contentType: "application/json"
    });
    $.post({
        url  : "/login",
        data : '{"username": "'+username+'", "password": "'+password+'"}'
    }).done(function(data) {
        window.location.replace("/home");
    }).fail(function(xhr, status, error) {
        if (xhr.status === 401) {
            document.getElementById("error").innerHTML = "Invalid username or password!";
        } else {
            document.getElementById("error").innerHTML = 'Error!<br/>' + xhr.status + ': ' + error;
        }
    });
}

function forgot_password() {
    document.getElementById("error").innerHTML = "TODO!"; //TODO
}