function on_load() {
    document.getElementById("update_password_form").addEventListener("submit", function(event) {
        //form submit does not support json
        event.preventDefault();
        update_password();
    });
}

function update_password() {
    let username = document.getElementById("username").value;
    let current_password = document.getElementById("current_password").value; //TODO: actually send this lol
    let new_password = document.getElementById("new_password").value;
    $.ajaxSetup({
        contentType: "application/json"
    });
    $.ajax({
        method: 'PUT',
        url  : "/users/"+username,
        data : '{"username": "'+username+'", "password": "'+new_password+'"}'
    }).done(function(data) {
        document.getElementById("error").innerHTML = "DONE!";
    }).fail(function(xhr, status, error) {
        if (xhr.status === 401 || xhr.status === 404) {
            document.getElementById("error").innerHTML = "Invalid username or password!";
        } else {
            document.getElementById("error").innerHTML = 'Error!<br/>' + xhr.status + ': ' + error;
        }
    });
}