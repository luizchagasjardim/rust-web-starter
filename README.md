# Capabilities

* Rest api for user
* Passwords are encrypted
* User login, logout, cookies for session

# Setup 

TODO

# Run

Have postgres running.

Run `redis-server` on a terminal.

Run `cargo +nightly run` on another terminal.

Go to [localhost:8000/home](http://localhost:8000/home)