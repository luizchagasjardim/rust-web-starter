#![feature(proc_macro_hygiene, decl_macro)]

pub mod db;
pub mod backend_result;
pub mod users;
pub mod session;
pub mod error_catchers;

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate diesel;
#[macro_use] extern crate serde_derive;
extern crate dotenv;

use rocket::response::Redirect;
use rocket_contrib::templates::Template;
use rocket_contrib::json::Json;
use db::models::*;
use db::connection::*;
use backend_result::BackendResult;
use backend_result::TemplateResult;
use session::Session;

#[get("/login")]
fn login_page() -> Template {
    let map = std::collections::HashMap::<String, String>::new(); //TODO: static page?
    Template::render("login", &map)
}

#[post("/login", data = "<credentials>")]
fn login(conn: DbConn, session: Session, credentials: Json<NewUser>) -> BackendResult<String> {
    use backend_result::BackendError;

    let credentials = credentials.into_inner();

    let user = User::find_by_name(&conn, &credentials.username)?;

    user.verify_password(credentials.password.as_bytes()).map_err(|e| {
        //TODO
        session.end_session();
        BackendError::DbError(e)
    })?;

    session.start_session(user.username.clone());

    Ok(Json(format!("Signed in as {}", user.username)))
}

#[post("/logout")]
fn logout(session: Session) -> TemplateResult {
    use backend_result::BackendError;

    let optional_user = session.get_username();

    match optional_user {
        Some(_) => {
            session.end_session();
            Ok(login_page())
        },
        None => Err(BackendError::Unauthorized)
    }
}

#[get("/home")]
fn home(session: Session) -> Result<Template, Redirect> {
    let optional_user = session.get_username();

    match optional_user {
        Some(user) => {
            let mut map = std::collections::HashMap::new();
            map.insert("username", user);
            Ok(Template::render("home", &map))
        },
        None => Err(Redirect::to(uri!(login)))
    }

}

#[get("/update_password")]
fn update_password_page(session: Session) -> Template {
    Template::render("update_password", session.as_context()) //TODO: use std::convert::identity here?
}

fn main() {
    use rocket_contrib::serve::StaticFiles;
    use users::MountUsers;
    use error_catchers::MountErrorCatchers;

    rocket::ignite()
        .attach(Template::fairing())
        .attach(DbConn::fairing())
        .attach(UserSessionsDbConn::fairing())
        .attach(Session::fairing().with_cookie_name("saa_session"))
        .mount("/", StaticFiles::from("static"))
        .mount_user_api("/users")
        .mount("/", routes![
            login_page,
            login,
            logout,
            home,
            update_password_page,
            ])
        .mount_error_catchers()
        .launch();
}