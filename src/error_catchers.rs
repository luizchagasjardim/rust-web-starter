use rocket::Rocket;
use rocket::Request;
use rocket_contrib::templates::Template;

fn show_error(error: &'static str, request: &Request) -> Template {
    use std::collections::HashMap;

    let mut map = HashMap::new();

    map.insert("error", error);

    map.insert("method", request.method().as_str());

    map.insert("path", request.uri().path());

    map.insert("query", match request.uri().query() {
        Some(query) => query,
        None => ""
    });

    let segment_count = request.uri().segment_count().to_string();
    map.insert("segment_count", &segment_count);

    let headers = format!("{:#?}", request.headers());
    map.insert("headers", &headers);

    let remote = match request.remote() {
        Some(value) => format!("{:?}", value),
        None => "".to_string()
    };
    map.insert("remote", &remote);

    Template::render("error", &map)
}

#[catch(400)]
fn bad_request(request: &Request) -> Template {
    show_error("400: Bad Request", request)
}

#[catch(404)]
fn not_found(request: &Request) -> Template {
    show_error("404: Page Not Found", request)
}

#[catch(422)]
fn unprocessable_entity_error(request: &Request) -> Template {
    show_error("422: Unprocessable Entity", request)
}

#[catch(500)]
fn internal_server_error(request: &Request) -> Template {
    show_error("500: Internal Server Error", request)
}

pub trait MountErrorCatchers {
    fn mount_error_catchers(self) -> Self;
}

impl MountErrorCatchers for Rocket {
    fn mount_error_catchers(self) -> Self {
        self.register(catchers![
            bad_request,
            not_found,
            internal_server_error,
            unprocessable_entity_error,
            ])
    }
}