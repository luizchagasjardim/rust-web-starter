use rocket::request::{FromRequest, Outcome};
use rocket::Request;
use crate::db::connection::DbConn;
use crate::db::models::User;
use crate::backend_result::BackendError;

type Map = std::collections::HashMap<String, String>;
type Inner<'a> = rocket_session::Session<'a, Map>;
type Fairing =  rocket_session::SessionFairing::<Map>;

pub struct Session<'a>(Inner<'a>);

impl Session<'_> {
    pub fn get_username(&self) -> Option<String> {
        self.0.tap(|map| {
            map.get("username")
                .map(|x| x.clone())
        })
    }
    pub fn start_session(&self, username: String) -> Option<String> {
        self.0.tap(|map| {
            map.insert("username".to_string(), username)
        })
    }
    pub fn end_session(&self) -> Option<String> {
        self.0.tap(|map| -> Option<String> {
            map.remove("username")
        })
    }
    pub fn as_context(&self) -> Map {
        self.0.tap(|map| {map.clone()})
    }
    pub fn fairing() -> Fairing {
        Inner::fairing()
    }
    pub fn login_check(&self) -> Result<String, BackendError> {
        match self.get_username() {
            None => Err(BackendError::Unauthorized),
            Some(username) => Ok(username)
        }
    }
    pub fn admin_check(&self, conn: &DbConn) -> Result<(), BackendError> {
        let username = self.login_check()?;
        let is_admin = User::is_admin(&conn, &username)?;
        if is_admin {
            Ok(())
        } else {
            Err(BackendError::Unauthorized)
        }
    }
}

impl<'a> From<Inner<'a>> for Session<'a> {
    fn from(values: Inner<'a>) -> Self {
        Self(values)
    }
}

impl<'a, 'r> FromRequest<'a, 'r> for Session<'a> {
    type Error = <Inner<'a> as FromRequest<'a, 'r>>::Error;

    fn from_request(request: &'a Request<'r>) -> Outcome<Self, Self::Error> {
        Inner::from_request(request).map(|x| Self::from(x))
    }
}