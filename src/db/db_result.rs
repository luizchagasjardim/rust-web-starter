use diesel;
use argon2;

#[derive(Debug)]
pub enum DbError {
    DieselError(diesel::result::Error),
    ArgonError(argon2::Error),
    UsernameNotFound(String),
    InvalidPassword,
    Other(String),
}

impl From<diesel::result::Error> for DbError {
    fn from(e: diesel::result::Error) -> Self {
        DbError::DieselError(e)
    }
}

impl From<argon2::Error> for DbError {
    fn from(e: argon2::Error) -> Self {
        DbError::ArgonError(e)
    }
}

pub type DbResult<T> = std::result::Result<T, DbError>;
