use super::schema::users;
use super::connection::*;
use super::schema::users::dsl::users as user_table;
use super::db_result::DbResult;
use diesel::prelude::*;
use crate::db::db_result::DbError;

#[derive(Queryable, Serialize)]
pub struct User {
    pub username: String,
    #[serde(skip_serializing)]
    pub password: String,
    pub is_admin: bool,
}

impl User {

    pub fn list(conn: &DbConn) -> DbResult<Vec<User>> {
        let user_list = user_table.load::<User>(&conn.0)?;
        Ok(user_list)
    }

    pub fn find_by_name(conn: &DbConn, name: &String) -> DbResult<User> {
        use users::dsl::username;
        let user = user_table
            .filter(username.eq(name))
            .get_result::<User>(&conn.0)
            .map_err(|e| {
                match e {
                    diesel::result::Error::NotFound => DbError::UsernameNotFound(name.clone()),
                    _ => DbError::DieselError(e)
                }
            })?;
        Ok(user)
    }

    pub fn insert(conn: &DbConn, user: NewUser) -> DbResult<User> {
        user.insert(&conn)
    }

    pub fn update(conn: &DbConn, user: NewUser) -> DbResult<User> {
        user.update(&conn)
    }

    pub fn delete(conn: &DbConn, name: &String) -> DbResult<usize> {
        use users::dsl::username;
        let deleted = diesel::delete(
            user_table.filter(username.eq(&name))
        ).execute(&conn.0)?;
        Ok(deleted)
    }

    pub fn verify_password(&self, password: &[u8]) -> DbResult<()> {
        use super::db_result::DbError::InvalidPassword;
        let validation = argon2::verify_encoded(&self.password, password)?;
        if !validation {
            Err(InvalidPassword)
        } else {
            Ok(())
        }
    }

    pub fn is_admin(conn: &DbConn, name: &String) -> DbResult<bool> {
        let user = User::find_by_name(&conn, name)?;
        Ok(user.is_admin)
    }

}

#[derive(Insertable, Deserialize, AsChangeset)]
#[table_name="users"]
pub struct NewUser {
    pub username: String,
    pub password: String,
}

impl From<NewUser> for User {
    fn from(user: NewUser) -> Self {
        User {
            username: user.username,
            password: user.password,
            is_admin: false,
        }
    }
}

impl NewUser {

    pub fn insert(mut self, conn: &DbConn) -> DbResult<User> {
        self.hash_password()?;

        let insert_result = diesel::insert_into(users::table)
            .values(self)
            .get_result::<User>(&conn.0)?;

        Ok(insert_result)
    }

    pub fn update(mut self, conn: &DbConn) -> DbResult<User> {
        use users::dsl::username;
        self.hash_password()?;
        let user = diesel::update(user_table)
            .filter(username.eq(self.username.clone()))
            .set(self)
            .get_result(&conn.0)?;
        Ok(user)
    }

    fn hash_password(&mut self) -> DbResult<()> {
        use argon2::Config;
        use rand::Rng;

        let salt: [u8; 32] = rand::thread_rng().gen();
        let config = Config::default();

        self.password = argon2::hash_encoded(
            self.password.as_bytes(),
            &salt,
            &config
        )?;

        Ok(())
    }

}
