use rocket::Rocket;
use rocket_contrib::json::Json;
use crate::db::models::*;
use crate::db::connection::*;
use crate::backend_result::*;
use crate::session::Session;

#[get("/")]
pub fn user_list(conn: DbConn, session: Session) -> BackendResult<Vec<User>> {
    session.admin_check(&conn)?;
    let list = User::list(&conn)?;
    Ok(Json(list))
}

#[post("/", data = "<user>")]
pub fn create_user<'a>(conn: DbConn, user: Json<NewUser>) -> BackendResult<User> {
    let user = user.into_inner();
    let user = user.insert(&conn)?;
    Ok(Json(user))
}

#[get("/<name>")]
pub fn find_user(conn: DbConn, name: String, session: Session) -> BackendResult<User> {
    session.login_check()?;
    let user = User::find_by_name(&conn, &name)?;
    Ok(Json(user))
}

#[put("/<name>", data = "<user>")]
pub fn update_user_password(conn: DbConn, name: String, user: Json<NewUser>, session: Session) -> BackendResult<User> {
    use crate::backend_result::BackendError;

    let username = session.login_check()?;
    let user = user.into_inner();
    if user.username != name || username != name {
        return Err(BackendError::Unauthorized);
    }

    let user = user.update(&conn)?;
    Ok(Json(user))
}

#[delete("/<name>")]
pub fn delete_user(conn: DbConn, name: String, session: Session) -> BackendResult<usize> {
    let username = session.login_check()?;
    if username != name {
        session.admin_check(&conn)?;
    }
    let deleted = User::delete(&conn, &name)?;
    Ok(Json(deleted))
}

pub trait MountUsers {
    fn mount_user_api(self, base: &str) -> Self;
}

impl MountUsers for Rocket {
    fn mount_user_api(self, base: &str) -> Self {
        self.mount(base, routes![
            user_list,
            create_user,
            find_user,
            update_user_password,
            delete_user
            ])
    }
}